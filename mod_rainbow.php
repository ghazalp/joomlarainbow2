<?php
/**
 * @version		$Id: mod_rainbow.php 20196 2012-04-02 
 * @subpackage	mod_rainbow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 modulesDSmod_rainbowDScssDSgithub.css
 
(JURI::root(true), 'images');
 
 */

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.framework', true);

if ($params->def('prepare_content', 1))
{
	JPluginHelper::importPlugin('content');
	$module->content = JHtml::_('content.prepare', $module->content);
}

$css_file = $params->get( 'stylesheet', 'github.css' );
if ( $css_file ) { 
	
	$css_url =  JURI::root(true).'/modules/mod_rainbow/css/'. $css_file;
	$document = JFactory::getDocument();
	$document->addStyleSheet ( $css_url );
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_rainbow', $params->get('layout', 'default'));