<?php
/**
 * @version		$Id: default.php 
 * @package		Joomla.Site
 * @subpackage	mod_rainbow
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
defined('_JEXEC') or die;
JHtml::_('behavior.framework', true);
$document = JFactory::getDocument();
$document->addScript( JURI::base().'modules/mod_rainbow/assets/rainbow_min.js' );
?>