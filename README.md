mod_rainbow for joomla
==============
This module is a Code Highlighter using Rainbow small JavaScript library.<br />
https://github.com/ccampbell/rainbow
<p>Joomla 2.5 and 3.0 compatible</p>
USAGE :

Select a position. The simplest method is to create a rainbow position.

Publish everywhere.

In the article, add the following tags, depending on language :<br />
CSS<br />
&lt;pre&gt;&lt;code data-language=&quot;css&quot;&gt;Your code&lt;/code&gt;&lt;/pre&gt;<br />
JAVASCRIPT<br />
&lt;pre&gt;&lt;code data-language=&quot;javascript&quot;&gt;Your code&lt;/code&gt;&lt;/pre&gt;<br />
PHP<br />
&lt;pre&gt;&lt;code data-language=&quot;php&quot;&gt;Your code&lt;/code&gt;&lt;/pre&gt;<br />
HTML<br />
&lt;pre&gt;&lt;code data-language=&quot;html&quot;&gt;Your code&lt;/code&gt;&lt;/pre&gt;<br />

BE CAREFUL TO ENCODE ENTITIES FOR HTML.

<p><a href='http://htmlentities.net/'>An online encoder</a></p>

Don't forget to close tags.

Then, in the article, insert {loadposition rainbow}.

It is possible to choose your layout by selecting CSS files in Basic Options.
